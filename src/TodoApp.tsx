import React, { useState, useEffect } from "react";
import {
  Container,
  Typography,
  TextField,
  IconButton,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Checkbox,
  Divider,
  Button,
} from "@mui/material";
import { Add as AddIcon, Delete as DeleteIcon } from "@mui/icons-material";
import { Todo, TodoService } from "./TodoService";
import { ToastContainer } from "react-toastify";
import Toast from "./Toast";

const TodoApp: React.FC = () => {
  const [todos, setTodos] = useState<Todo[]>([]);
  const [todoInput, setTodoInput] = useState<string>("");

  useEffect(() => {
    fetchTodos();
  }, []);

  const fetchTodos = async () => {
    try {
      const data = await TodoService.getAllTodos();
      setTodos(data);
    } catch (error) {
      Toast.error("Failed to fetch todos. Please try again later.");
    }
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTodoInput(event.target.value);
  };

  const handleAddTodo = async () => {
    if (todoInput.trim() !== "") {
      try {
        const newTodo = await TodoService.createTodo(todoInput, todoInput);
        if (newTodo) {
          setTodos([...todos, newTodo]);
          setTodoInput("");
          Toast.success("A new todo has been successfully added.");
        }
      } catch (error) {
        Toast.error("Failed to add new todo.");
      }
    }
  };

  const handleToggleComplete = async (id: number) => {
    try {
      const updatedTodo = await TodoService.updateTodo(id, { completed: true });
      if (updatedTodo) {
        const updatedTodos = todos.map((todo) =>
          todo.id === id ? updatedTodo : todo
        );
        setTodos(updatedTodos);
      }
    } catch (error) {
      Toast.error("Failed to change the status");
    }
  };

  const handleDeleteTodo = async (id: number) => {
    try {
      const success = await TodoService.deleteTodo(id);
      if (success) {
        const updatedTodos = todos.filter((todo) => todo.id !== id);
        setTodos(updatedTodos);
        Toast.success("The todo has been successfully deleted.");
      }
    } catch (error) {
      Toast.error("Failed to delete todo");
    }
  };

  const handleDeleteAllTodo = async () => {
    try {
      const clearAll = todos.map((todo) => TodoService.deleteTodo(todo.id));
      await Promise.allSettled(clearAll);
      fetchTodos();
    } catch (error) {
      Toast.error("Failed to delete all todos.");
    }
  };

  return (
    <Container maxWidth="sm">
      <Typography variant="h4" align="center" gutterBottom sx={{ mt: 4 }}>
        Todo App
      </Typography>
      <TextField
        label="Add Todo"
        variant="outlined"
        fullWidth
        value={todoInput}
        onChange={handleInputChange}
        onKeyDown={(e) => {
          if (e.key === "Enter") {
            handleAddTodo();
          }
        }}
        InputProps={{
          endAdornment: (
            <IconButton onClick={handleAddTodo}>
              <AddIcon />
            </IconButton>
          ),
        }}
      />
      <List>
        {todos.map((todo) => (
          <React.Fragment key={todo.id}>
            <ListItem>
              <Checkbox
                checked={todo.completed}
                onChange={() => handleToggleComplete(todo.id)}
              />
              <ListItemText primary={todo.title} secondary={todo.description} />
              <ListItemSecondaryAction>
                <IconButton onClick={() => handleDeleteTodo(todo.id)}>
                  <DeleteIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
          </React.Fragment>
        ))}
      </List>
      <Button onClick={() => handleDeleteAllTodo()} color="secondary" fullWidth>
        Clear All Todos
      </Button>
      <ToastContainer />
    </Container>
  );
};

export default TodoApp;
