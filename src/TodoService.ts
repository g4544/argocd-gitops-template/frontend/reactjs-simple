export interface Todo {
  id: number;
  title: string;
  description?: string;
  completed: boolean;
}

const baseUrl: string = String(
  process.env.REACT_APP_BASE_URL || "http://localhost:4000/"
);

export const TodoService = {
  async getAllTodos(): Promise<Todo[]> {
    const response = await fetch(`${baseUrl}todo`);
    if (!response.ok) {
      throw new Error("Failed to fetch todos");
    }
    return response.json();
  },

  async createTodo(title: string, description: string): Promise<Todo> {
    const response = await fetch(`${baseUrl}todo`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ title, description }),
    });
    if (!response.ok) {
      throw new Error("Failed to create todo");
    }
    return response.json();
  },

  async updateTodo(todoId: number, todoData: Partial<Todo>): Promise<Todo> {
    const response = await fetch(`${baseUrl}todo/${todoId}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(todoData),
    });
    if (!response.ok) {
      throw new Error("Failed to update todo");
    }
    return response.json();
  },

  async deleteTodo(todoId: number): Promise<boolean> {
    const response = await fetch(`${baseUrl}todo/${todoId}`, {
      method: "DELETE",
    });
    if (!response.ok) {
      throw new Error("Failed to delete todo");
    }
    return true;
  },
};
